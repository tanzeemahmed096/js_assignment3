//Creating string 3 function
module.exports = function string3(dateStr) {
  //If no date is provided
  if (arguments.length === 0) return "Date is not provided";

  //Array consisting of all months
  const monthArr = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  //Creating date object with the given date value
  const newDate = new Date(dateStr);
  if (isNaN(newDate.getUTCMonth())) return "Date format is wrong";

  //Returning the month;
  return monthArr[newDate.getMonth()];
};
