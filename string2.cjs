//Creating the string 2 function
module.exports = function string2(str) {
  //if no parameters given the return empty array
  if (arguments.length === 0) return [];

  //Using split function to split the string
  const resArr = str.split(".");

  //Converting the values to number
  for (let idx = 0; idx < resArr.length; idx++) {
    const numVal = Number(resArr[idx]);

    //Checking if it has some other characters
    if (isNaN(numVal)) return [];
    else resArr[idx] = numVal;
  }

  //Returning the resultant array
  return resArr;
};
