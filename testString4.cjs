//Importing the string4 function
const strFunc = require('./string4.cjs');

let obj = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};
//Calling string4 function
const fullName = strFunc(obj);

console.log(fullName);