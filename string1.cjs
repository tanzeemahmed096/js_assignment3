//Creating string1 function and exporting
module.exports = function string1(value) {
  //If no arguments provided
  if (arguments.length === 0) {
    return 0;
  }

  const numArr = [];
  //Iterating over arguments of string
  for (let idx = 0; idx < arguments.length; idx++) {
    //storing the string in a variable at argument index
    const str = arguments[idx];
    //if the argument is not a string
    if(typeof str !== "string"){
        numArr.push(0);
        continue;
    }
    //Variable to remove all the leading spaces
    let count = 0;
    //to check the sign of the number
    let sign = 1;

    //removing leading spaces
    while (count < str.length && str.charAt(count) === " ") count++;

    //if $ sign is given with the number then removing that
    if (count < str.length && str.charAt(count) === "$") count++;

    //checking the sign of the number
    if (
      count < str.length &&
      (str.charAt(count) === "-" || str.charAt(count) === "+")
    ) {
      str.charAt(count) === "+" ? (sign = 1) : (sign = -1);
      count++;
    }

    if (count < str.length && str.charAt(count) === "$") count++;

    //taking string which has to be converted
    const resNum = Number(str.substring(count, str.length));

    //pushing 0 if it is not a number else pushing the number
    if (resNum === NaN) numArr.push(0);
    else numArr.push(sign * resNum);
  }

  return numArr;
};
