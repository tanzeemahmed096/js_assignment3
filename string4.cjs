//Creating a string 4 function
module.exports = function string4(nameObj) {
  //checking if not arguments given or argument is not an object
  if (arguments.length === 0 || typeof nameObj !== "object" || JSON.stringify(nameObj) === "{}") {
    return "No data provided or empty data";
  }

  //Getting the full name from the object
  let fname = (nameObj.first_name) ? nameObj.first_name : "";
  let mname = (nameObj.middle_name) ? nameObj.middle_name : "";
  let lname = (nameObj.last_name) ? nameObj.last_name : "";

  //Converting first character to upper case and other chars to lower case
  fname = fname.substring(0,1).toUpperCase() + fname.substring(1, fname.length).toLowerCase();
  mname = mname.substring(0,1).toUpperCase() + mname.substring(1, mname.length).toLowerCase();
  lname = lname.substring(0,1).toUpperCase() + lname.substring(1, lname.length).toLowerCase();

  return `${fname} ${mname} ${lname}`;
};
