//Creating function for problem 5
module.exports = function string5(strArr) {
  //Checking if the arguments are not given or it is an empty array
  if (arguments.length === 0 || strArr.length === 0) return [];

  //Joining the string arrays using join function
  const resStr = strArr.join(" ");
  
  //return the resultant string
  return resStr;
};
